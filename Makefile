include config.mk

SRC  = git-fs.scm
EXT  = fuse git matchable type-extensions srfi-13
AUX  = LICENSE README Makefile config.mk
BIN  = $(SRC:.scm=)
OBJ  = $(SRC:.scm=.o)
LINK = $(SRC:.scm=.link)
MAN  = $(SRC:.scm=.1)
DIST = $(NAME)-$(VERSION)
TGZ  = $(DIST).tar.gz
ARCH = $(shell dpkg-architecture -qDEB_BUILD_ARCH)
DEB  = $(NAME)_$(VERSION)_$(ARCH).deb
DEPS = $(EXT:=.o)

GPG  = gpg
ENV  = env VERSION=$(VERSION) MOUNT_PATH=$(MOUNT_PATH)

CHICKEN_INSTALL_REPOSITORY = $(CURDIR)
CHICKEN_REPOSITORY_PATH = $(CHICKEN_INSTALL_REPOSITORY):$(CHICKEN_REPOSITORY)

FPM = fpm
URL = https://git.foldling.org/git-fs.git/
CONTACT = Evan Hanson <evhan@foldling.org>
PACKAGING = --name "$(NAME)" \
	    --version "$(VERSION)" \
	    --maintainer "$(CONTACT)" \
	    --vendor "$(CONTACT)" \
	    --url "$(URL)" \
	    --license BSD \
	    --category devel \
	    --architecture native \
	    --description "git filesystem interface" \
	    --depends "libc6 (>= 2.3)" \
	    --depends "fuse (>= $(LIBFUSE))" \
	    --depends "libfuse2 (>= $(LIBFUSE))" \
	    --depends "libgit2-26 (>= $(LIBGIT2))"

CSCFLAGS += -X type-extensions
CSCFLAGS += $(shell pkg-config --cflags fuse libgit2 | xargs -d' ' -I{} printf ' -C %s' {})
CSCFLAGS += $(shell pkg-config --libs fuse libgit2 | xargs -d' ' -I{} printf ' -L %s' {})

export CHICKEN_INSTALL_REPOSITORY
export CHICKEN_REPOSITORY_PATH

.SUFFIXES:
.SUFFIXES: .1 .1.md .scm .o .so

.PHONY: all clean deb deps dist doc example install uninstall

all: $(BIN) $(MAN)

deb: $(DEB)

doc: $(MAN)

deps: $(DEPS)

git-fs: git-fs.scm $(DEPS)
	@echo compiling binary $@
	@$(ENV) $(CSC) $(CSCFLAGS) $< -o $@

.1.md.1:
	@echo generating manual page $@
	@cat $< \
	   | sed 's|VERSION|$(VERSION)|g' \
	   | sed 's|MOUNT_PATH|$(MOUNT_PATH)|g' \
	   | pandoc -f markdown -t man -s > $@

$(DEPS):
	@echo installing build dependency $(basename $@ .o)
	@$(CHICKEN_INSTALL) $(basename $@ .o) > /dev/null

$(DEB): $(BIN) $(MAN)
	@echo creating debian package $@
	@mkdir -p $(BUILDDIR)$(BINPREFIX) $(BUILDDIR)$(MANPREFIX)
	@cp -f $(BIN) $(BUILDDIR)$(BINPREFIX)
	@cp -f $(MAN) $(BUILDDIR)$(MANPREFIX)
	@$(FPM) -f -p $@ -s dir -t deb -C $(BUILDDIR) $(PACKAGING) . > /dev/null

$(TGZ): $(AUX) $(MAN) $(SRC)
	@echo creating distribution tarball
	@mkdir -p $(DIST)
	@cp $(AUX) $(MAN) $(SRC) $(DIST)
	@tar -czf $(TGZ) $(DIST)
	@rm -fr $(DIST)

dist: $(TGZ)
	@echo signing distribution tarball
	@$(GPG) --detach-sign $<
	@$(GPG) --detach-sign --armor $<

install: $(BIN) $(MAN)
	@echo installing binary $(BIN) to $(DESTDIR)$(PREFIX)/bin
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@cp -f $(BIN) $(DESTDIR)$(PREFIX)/bin
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/$(BIN)
	@echo installing manual page $(MAN) to $(DESTDIR)$(MANPREFIX)/man1
	@mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	@cp -f $(MAN) $(DESTDIR)$(MANPREFIX)/man1/$(MAN)
	@chmod 644 $(DESTDIR)$(MANPREFIX)/man1/$(MAN)

uninstall:
	@echo removing binary from $(DESTDIR)$(PREFIX)/bin
	@rm -f $(DESTDIR)$(PREFIX)/bin/$(BIN)
	@echo removing manual page from $(DESTDIR)$(MANPREFIX)/man1
	@rm -f $(DESTDIR)$(MANPREFIX)/man1/$(MAN)

example: $(BIN)
	@sh < example.sh | sed 's/^/    /'

clean:
	@echo cleaning source tree
	@rm -fr *.o *.so *.link *.inline *.types *.egg-info *.import.scm
	@rm -fr $(BIN) $(DEB) $(MAN) $(TGZ) $(BUILDDIR) $(TGZ).sig $(TGZ).asc

git-fs
======
A filesystem interface for Git.

Installation
------------
Requirements:

 * [GNU make](https://www.gnu.org/software/make/)
 * [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/)
 * [libfuse](https://github.com/libfuse/libfuse/)
 * [libgit2](https://libgit2.github.com/)
 * [CHICKEN](https://call-cc.org/)
    * [fuse](https://eggs.call-cc.org/5/fuse)
    * [git](https://eggs.call-cc.org/5/git)
    * [matchable](https://eggs.call-cc.org/5/matchable)
    * [srfi-13](https://eggs.call-cc.org/5/srfi-13)
    * [type-extensions](https://eggs.call-cc.org/5/type-extensions)

Source tarballs and binary packages for Debian-based systems are
available at <https://git.foldling.org/git-fs/dist/>.

To build the program from source:

    $ make deps
    $ make all

To install the program and its manual page:

    $ make install

Usage
-----
`git-fs` serves the current repository at the specified mountpoint, or
`.git/fs` if no mountpoint is given.

    git-fs [-v] [[<repository>] <mountpoint>]

For example:

    $ ./git-fs &
    $ ls .git/fs
    commits
    refs
    tags
    $ ls .git/fs/refs/heads/master
    diff
    header
    message
    tree
    $ cat .git/fs/refs/heads/master/message
    Port to CHICKEN 5
    $ fusermount -u .git/fs

Copyright
---------
Copyright (c) 2014-2019 Evan Hanson <evhan@foldling.org>

BSD-style license. See LICENSE for details.

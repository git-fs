NAME       = git-fs
VERSION    = 0.0.5
MOUNT_PATH = .git/fs

LIBFUSE    = 2.9.0
LIBGIT2    = 0.26.0

PREFIX     = /usr/local
MANPREFIX  = $(PREFIX)/share/man/man1
LIBPREFIX  = $(PREFIX)/lib
BINPREFIX  = $(PREFIX)/bin
BUILDDIR   = build

CSC        = csc
CSCFLAGS   = -d0 -O5 -strict-types -static

CHICKEN_INSTALL = chicken-install
CHICKEN_REPOSITORY = $(shell $(CHICKEN_INSTALL) -repository)

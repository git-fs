#!/bin/sh

set -e

alias ls='ls -1'

run() {
  while read line
  do
    echo "$ $line"
    eval "$line"
    sleep 1
  done
}

cat <<SCRIPT | run
./git-fs &
ls .git/fs
ls .git/fs/refs/heads/master
cat .git/fs/refs/heads/master/message
fusermount -u .git/fs
SCRIPT

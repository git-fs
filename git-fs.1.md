% GIT-FS(1) git-fs User Manual | Version VERSION
% Evan Hanson <evhan@foldling.org>
% June 2019

# NAME

**git-fs** - Git filesystem.

# SYNOPSIS

**git-fs** \[*option* ...] [[*repository*] *mountpoint*]

# DESCRIPTION

**git-fs** mounts a Git repository as a local filesystem.

If no *mountpoint* is specified, `MOUNT_PATH` is used.

If no *repository* is specified, the current working directory is used.

# OPTIONS

-h
:   Print usage information to standard output and exit.

-v
:   Print version information to standard output and exit.

# CAVEATS

Many filesystem operations, including `statvfs`(2), are unimplemented.

File access, modification, and change times reflect the time and
timezone of the parent commit or tag rather than the time of the last
change to that specific file. If there is no commit or tag associated
with a file, filesystem mount time is used.

Recursive tags are ignored.

# SEE ALSO

`git`(1), `fuse`(8)

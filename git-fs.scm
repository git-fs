;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; git-fs.scm - A Git filesystem.
;;;
;;; http://www.git-scm.com/
;;; http://libgit2.github.com/
;;; http://fuse.sourceforge.net/
;;;
;;; Copyright © 2014-2019 Evan Hanson. All Rights Reserved.
;;; BSD-style license. See LICENSE for details.
;;;

(import (chicken bitwise)
        (chicken blob)
        (chicken condition)
        (chicken errno)
        (chicken file)
        (chicken file posix)
        (chicken format)
        (chicken io)
        (chicken process signal)
        (chicken process-context)
        (chicken process-context posix)
        (chicken string)
        (chicken time)
        (chicken type)
        (fuse)
        (srfi 1)
        (srfi 13)
        (rename (git)
                (tree-ref tree-ref/path)
                (index-ref index-ref/path)
                (reference-target reference-target/oid)))

(import-syntax (matchable))

(import-for-syntax (srfi 1)
                   (chicken format)
                   (chicken process-context))

;;;
;;; Build variables.
;;;

(define-syntax define-build-variable
  (ir-macro-transformer
   (lambda (e i c)
     (let ((name (second e))
           (var  (third e)))
       `(define-constant ,name
          ,(or (get-environment-variable var)
               (error (format "no ~a specified" var))))))))

(define-build-variable program-version "VERSION")
(define-build-variable default-mount-path "MOUNT_PATH")

;;;
;;; Utilities.
;;;

(define-type (nonnull-list-of x) (list x . x))

(define-type nonnull-list (nonnull-list-of *))

(: nonnull (forall (a) ((#!rest -> (list-of a)) -> (#!rest -> (or (nonnull-list-of a) false)))))
(define ((nonnull f) . a)
  (let ((r (apply f a)))
    (and (not (null? r)) r)))

(: print-error (#!rest -> void))
(define (print-error . args)
  (apply fprintf (current-error-port) args))

(: die (#!rest -> noreturn))
(define (die . args)
  (apply print-error args)
  (newline (current-error-port))
  (exit 1))

;;;
;;; Type aliases.
;;;

(define-type blob*      (struct blob))
(define-type commit     (struct commit))
(define-type diff       (struct diff))
(define-type oid        (struct oid))
(define-type reference  (struct reference))
(define-type repository (struct repository))
(define-type tag        (struct tag))
(define-type tree       (struct tree))
(define-type tree-entry (struct tree-entry))
(define-type filesystem (struct filesystem))

;;;
;;; Directory listing.
;;;

(define-type directory-list
  (list string string . string))

(: directory-list ((list-of string) -> directory-list))
(define (directory-list entries)
  (append '("." "..") entries))

(: repository-references (repository string -> (list-of string)))
(define (repository-references repo path)
  (let* ((path/ (string-append path "/"))
         (len   (string-length path/)))
    (references-fold
     (lambda (r a)
       (let ((part (let ((name (string-drop (reference-name r) len)))
                     (cond ((string-index name #\/) =>
                            (cut string-take name <>))
                           (else name)))))
         (if (member part a)
             a
             (cons part a))))
     '()
     repo
     (string-append path/ "*"))))

(: tree-directory-list (tree (list-of string) -> directory-list))
(define (tree-directory-list tree parts)
  (directory-list
   (let ((path (string-join parts "/" 'suffix)))
     (tree-fold
      (lambda (path* entry lst)
        (cond ((string=? path* path)
               (cons (tree-entry-name entry) lst))
              (else lst)))
      '()
      tree))))

(: commit-directory-list (commit (list-of string) -> directory-list))
(define (commit-directory-list commit parts)
  (tree-directory-list (commit-tree commit) parts))

;;;
;;; File attributes.
;;;

(define-type stat
  (vector fixnum fixnum fixnum fixnum fixnum fixnum fixnum fixnum))

(: u+rwx/reg fixnum)
(define u+rwx/reg
  (bitwise-ior file/reg perm/irusr perm/iwusr perm/ixusr))

(: u+rwx/dir fixnum)
(define u+rwx/dir
  (bitwise-ior file/dir perm/irusr perm/iwusr perm/ixusr))

(: directory-stat (#!optional fixnum fixnum fixnum -> stat))
(define (directory-stat #!optional (mtime mount-time) (ctime mtime) (atime ctime))
  (vector u+rwx/dir
          2
          (current-user-id)
          (current-group-id)
          0
          atime
          ctime
          mtime))

(: regular-file-stat (fixnum #!optional fixnum fixnum fixnum -> stat))
(define (regular-file-stat size #!optional (mtime mount-time) (ctime mtime) (atime ctime))
  (vector u+rwx/reg
          1
          (current-user-id)
          (current-group-id)
          size
          ;; TODO
          atime
          ctime
          mtime))

;;;
;;; Git utilities.
;;;

(: repository-fetch (forall (a) (repository (repository #!rest -> a) #!rest -> (or a false))))
(define (repository-fetch repo fetcher . args)
  (condition-case
    (apply fetcher repo args)
    ((git) #f)))

(: commits/all-branches (repository -> (list-of commit)))
(define (commits/all-branches repo) (commits repo initial: (branches repo)))

(: commit-diff (commit -> diff))
(define (commit-diff c)
  (diff (commit-repository c)
        (cond ((commit-parent c) => commit-tree)
              (else #f))
        (commit-tree c)))

(: diff-length (diff -> fixnum))
(define (diff-length diff)
  (let ((len (diff-num-deltas diff)))
    (do ((i 0 (+ i 1))
         (n 0 (+ n (patch-size (diff-patch diff i)))))
        ((= i len) n))))

(: reference-target (reference -> (or blob* commit tag tree false)))
(define (reference-target ref)
  (repository-ref (reference-repository ref)
                  (reference-target/oid ref)))

(: tag-header (tag -> string))
(define (tag-header tag)
  (let ((s (tag-tagger tag)))
    (format "target ~a~ntagger ~a ~a ~a ~a~n"
            (oid->string (object-id (tag-target tag)))
            (signature-name s)
            (signature-email s)
            (signature-time s)
            (signature-time-offset s))))

(: tree-ref (tree (list-of string) -> (or tree-entry false)))
(define (tree-ref tree parts)
  (tree-ref/path tree (string-join parts "/")))

(: commit-ref (commit (list-of string) -> (or tree-entry false)))
(define (commit-ref c parts)
  (tree-ref (commit-tree c) parts))

(: reference-ref (reference (list-of string) -> (or tree-entry false)))
(define (reference-ref r parts)
  (and-let* ((c (reference-commit r)))
    (commit-ref c parts)))

(: reference-commit (reference -> (or commit false)))
(define (reference-commit r)
  (let ((repo (reference-repository r)))
    (or (repository-ref repo r 'commit)
        (and-let* ((t (repository-ref repo r 'tag)))
          (let ((o (tag-target t)))
            (and (commit? o) o))))))

(: tag-time (tag -> fixnum))
(define tag-time
  (compose signature-time tag-tagger))

(: open-input-blob (blob* -> input-port))
(define open-input-blob
  (compose open-input-string blob->string blob-content))

(: open-input-tree-entry (tree-entry -> input-port))
(define open-input-tree-entry
  (compose open-input-blob tree-entry->object))

(: blob-stat (blob* #!optional fixnum fixnum fixnum -> stat))
(define (blob-stat blob #!optional (mtime mount-time) (ctime mtime) (atime ctime))
  (regular-file-stat (blob-length blob) mtime ctime atime))

(: tree-entry-stat (tree-entry #!optional fixnum fixnum fixnum -> (or stat false)))
(define (tree-entry-stat entry #!optional (mtime mount-time) (ctime mtime) (atime ctime))
  (case (tree-entry-type entry)
    ((tree)   (directory-stat mtime ctime atime))
    ((blob)   (blob-stat (tree-entry->object entry) mtime ctime atime))
    ((commit) #f) ; Submodule.
    (else =>  (cut error "Unrecognized tree entry type" <>))))

;;;
;;; Filesystem callbacks.
;;;

(: make-git-filesystem (repository -> filesystem))
(define (make-git-filesystem repo)

  (: fetch (procedure #!rest -> *))
  (define (fetch fetcher . args)
    (apply repository-fetch repo fetcher args))

  (: repository-references/nonnull (repository string -> (or (nonnull-list-of string) false)))
  (define repository-references/nonnull (nonnull repository-references))

  (: decompose-reference-path (string procedure -> *))
  (define (decompose-reference-path path f)
    (let lp ((path path)
             (trail '()))
      (cond ((string=? path "refs") #f)
            ((fetch reference path) =>
             (lambda (ref)
               (f ref trail)))
            (else
             (let ((i (string-index-right path #\/)))
               (lp (string-take path i)
                   (cons (string-drop path (add1 i)) trail)))))))

  (: filesystem-readdir (string -> (or directory-list false)))
  (define (filesystem-readdir path)

    (: commit-readdir (commit (list-of string) -> (or directory-list false)))
    (define (commit-readdir c parts)
      (match parts
        (()
         (directory-list '("diff" "header" "message" "tree")))
        (("tree" . trail)
         (commit-directory-list c trail))
        (else #f)))

    (: tag-readdir (tag (list-of string) -> (or directory-list false)))
    (define (tag-readdir t parts)
      (let ((o (tag-target t)))
        (match parts
          (()
           (case (object-type o)
             ((blob commit tree) =>
              (lambda (type)
                (directory-list
                 (cons (symbol->string type)
                       '("header" "message" "name")))))
             (else #f)))
          (("commit" . trail)
           (and (commit? o)
                (commit-readdir o trail)))
          (("tree" . trail)
           (and (tree? o)
                (tree-directory-list o trail)))
          (else #f))))

    (match (string-split path "/")
      (()
       (directory-list '("refs" "commits" "tags")))
      (("refs")
       (directory-list (repository-references repo "refs")))
      (("refs" . _)
       (let ((path (string-drop path 1)))
         (or (repository-references/nonnull repo path)
             (decompose-reference-path
              path
              (lambda (ref trail)
                (let ((o (reference-target ref)))
                  (case (object-type o)
                    ((commit)
                     (commit-readdir o trail))
                    ((tag)
                     (tag-readdir o trail))
                    (else #f))))))))
      (("commits")
       (directory-list
        (map (compose oid->string commit-id)
             (commits/all-branches repo))))
      (("commits" sha1 . trail)
       (and-let* ((c (fetch commit sha1)))
         (commit-readdir c trail)))
      (("tags")
       (directory-list
        (filter-map
         (lambda (t)
           (case (object-type (tag-target t))
             ((blob tree commit)
              (oid->string (tag-id t)))
             (else #f))) ; TODO (tag).
         (tags repo))))
      (("tags" sha1 . trail)
       (and-let* ((t (fetch tag sha1)))
         (tag-readdir t trail)))
      (else #f)))

  (: filesystem-getattr (string -> (or stat false)))
  (define (filesystem-getattr path)

    (: commit-getattr (commit (list-of string) -> (or stat false)))
    (define (commit-getattr c parts)
      (match parts
        (()
         (directory-stat (commit-time c)))
        (("diff")
         (regular-file-stat (diff-length (commit-diff c)) (commit-time c)))
        (("header")
         (regular-file-stat (string-length (commit-header c)) (commit-time c)))
        (("message")
         (regular-file-stat (string-length (commit-message c)) (commit-time c)))
        (("tree")
         (directory-stat (commit-time c)))
        (("tree" . trail)
         (and-let* ((e (commit-ref c trail)))
           (tree-entry-stat e (commit-time c))))
        (else #f)))

    (: tag-getattr (tag (list-of string) -> (or stat false)))
    (define (tag-getattr t parts)
      (let ((o (tag-target t)))
        (match parts
          (()
           (directory-stat (tag-time t)))
          (("header")
           (regular-file-stat (string-length (tag-header t)) (tag-time t)))
          (("message")
           (regular-file-stat (string-length (tag-message t)) (tag-time t)))
          (("name")
           (regular-file-stat (string-length (tag-name t)) (tag-time t)))
          (("blob")
           (and (blob? o)
                (regular-file-stat (blob-length o) (tag-time t))))
          (("commit" . trail)
           (and (commit? o)
                (commit-getattr o trail)))
          (("tree" . trail)
           (and (tree? o)
                (if (null? trail)
                    (directory-stat (tag-time t))
                    (and-let* ((e (tree-ref o trail)))
                      (tree-entry-stat e (tag-time t))))))
          (else #f))))

    (match (string-split path "/")
      (()
       (directory-stat))
      (("refs")
       (directory-stat))
      (("refs" . _)
       (let ((path (string-drop path 1)))
         (if (repository-references/nonnull repo path)
             (directory-stat)
             (decompose-reference-path
              path
              (lambda (ref trail)
                (let ((o (reference-target ref)))
                  (case (object-type o)
                    ((commit)
                     (commit-getattr o trail))
                    ((tag)
                     (tag-getattr o trail))
                    (else #f))))))))
      (("commits")
       (directory-stat))
      (("commits" sha1 . trail)
       (and-let* ((c (fetch commit sha1)))
         (commit-getattr c trail)))
      (("tags")
       (directory-stat))
      (("tags" sha1 . trail)
       (and-let* ((t (fetch tag sha1)))
         (tag-getattr t trail)))
      (else #f)))

  (: filesystem-open (string fixnum -> (or input-port false)))
  (define (filesystem-open path mode)

    (: commit-open (commit (list-of string) -> (or input-port false)))
    (define (commit-open c parts)
      (match parts
        (("diff")
         (open-input-string (diff->string (commit-diff c))))
        (("header")
         (open-input-string (commit-header c)))
        (("message")
         (open-input-string (commit-message c)))
        (("tree" . trail)
         (and-let* ((e (commit-ref c trail)))
           (case (tree-entry-type e)
             ((blob)
              (open-input-tree-entry e))
             (else #f))))
        (else #f)))

    (: tag-open (tag (list-of string) -> (or input-port false)))
    (define (tag-open t parts)
      (let ((o (tag-target t)))
        (match parts
          (("header")
           (open-input-string (tag-header t)))
          (("message")
           (open-input-string (tag-message t)))
          (("name")
           (open-input-string (tag-name t)))
          (("blob")
           (and (blob? o)
                (open-input-blob o)))
          (("commit" . trail)
           (and (commit? o)
                (commit-open o trail)))
          (("tree" . trail)
           (and (tree? o)
                (and-let* ((e (tree-ref o trail)))
                  (case (tree-entry-type e)
                    ((blob)
                     (open-input-tree-entry e))
                    (else #f)))))
        (else #f))))

    (match (string-split path "/")
      (("refs" . _)
       (decompose-reference-path
        (string-drop path 1)
        (lambda (ref trail)
          (let ((o (reference-target ref)))
            (case (object-type o)
              ((commit)
               (commit-open o trail))
              ((tag)
               (tag-open o trail))
              (else #f))))))
      (("commits" sha1 . trail)
       (and-let* ((c (fetch commit sha1)))
         (commit-open c trail)))
      (("tags" sha1 . trail)
       (and-let* ((t (fetch tag sha1)))
         (tag-open t trail)))
      (else #f)))

  (: filesystem-read (input-port fixnum fixnum -> string))
  (define (filesystem-read port size offset)
    (read-string size port))

  (: filesystem-release (input-port -> void))
  (define (filesystem-release port)
    (close-input-port port))

  (make-filesystem
   getattr: filesystem-getattr
   open:    filesystem-open
   read:    filesystem-read
   readdir: filesystem-readdir
   release: filesystem-release))

;;;
;;; Program start.
;;;

(: mount-time fixnum)
(define mount-time
  (inexact->exact (current-seconds)))

(: usage string)
(define usage
  (format "Usage: ~a [-v] [[<repository>] <mountpoint>]" (program-name)))

(: main ((list-of string) -> noreturn))
(define (main args)
  (match args
    (("-h")
     (print usage)
     (exit))
    (("-v")
     (print program-version)
     (exit))
    (()
     (let ((args* (list "." default-mount-path)))
       (if (directory-exists? default-mount-path)
           (main args*)
           (dynamic-wind
            (lambda () (create-directory default-mount-path))
            (lambda () (main args*))
            (lambda () (delete-directory default-mount-path))))))
    ((mount-path)
     (main (cons "." args)))
    ((repo-path mount-path)
     (let* ((repo     (condition-case (repository-open repo-path)
                        ((git) (die "No repository found at ~s" repo-path))))
            (fs       (make-git-filesystem repo))
            (fs-stop! (lambda _ (filesystem-stop! mount-path fs))))
       (set-signal-handler! signal/int fs-stop!)
       (set-signal-handler! signal/term fs-stop!)
       (with-exception-handler
        (lambda (e)
          (and-let* ((m (get-condition-property e 'exn 'message)))
            (print-error "Error: ~a~n" m))
          errno/io)
        (lambda ()
          (if (filesystem-start! mount-path fs)
              (filesystem-wait! mount-path fs)
              (die "Couldn't mount filesystem at ~s" mount-path))))))
    (else
     (die usage))))

(main (command-line-arguments))
